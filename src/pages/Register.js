import React, { useState } from "react";
import { register } from "../api";

export default function Register(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [salary, setSalary] = useState("");

  const save = async (e) => {
    e.preventDefault();
    let user = {
      username,
      password,
      name,
      age,
      salary,
    };
    try {
      const result = await register(user);
      console.log(result);
      if (result.data.status === "success") {
        return props.history.push("/home");
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <div className="d-flex justify-content-center">
        <div className=" register-form col-md-8">
          <div className="text-center">
            <img
              src={process.env.PUBLIC_URL + "/images/sun.svg"}
              width="120"
              alt="register-img"
            />
          </div>
          <h3 className="text-center mb-3">Register</h3>
          <form onSubmit={save}>
            <div className="form-group row pr-3 pl-3">
              <label
                className="col-sm-2 col-form-label"
                htmlFor="exampleInputUsername1"
              >
                Username:
              </label>
              <div className="col-md-10">
                {" "}
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputUsername1"
                  aria-describedby="emailHelp"
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group row pr-3 pl-3 ">
              <label
                className="col-sm-2 col-form-label"
                htmlFor="exampleInputPassword1"
              >
                Password:
              </label>
              <div className="col-md-10">
                {" "}
                <input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group row pr-3 pl-3">
              <label
                className="col-sm-2 col-form-label"
                htmlFor="exampleInputName1"
              >
                Name:
              </label>
              <div className="col-md-10">
                {" "}
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputName1"
                  onChange={(e) => setName(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group row pr-3 pl-3">
              <label
                className="col-sm-2 col-form-label"
                htmlFor="exampleInputAge1"
              >
                Age:
              </label>
              <div className="col-md-10">
                {" "}
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputAge1"
                  onChange={(e) => setAge(e.target.value)}
                />
              </div>
            </div>
            <div className="form-group row pr-3 pl-3">
              <label
                className="col-sm-2 col-form-label"
                htmlFor="exampleInputSalary1"
              >
                Salary:
              </label>
              <div className="col-md-10">
                <input
                  type="text"
                  className="form-control"
                  id="exampleInputSalary1"
                  onChange={(e) => setSalary(e.target.value)}
                />
              </div>
            </div>
            <button
              type="submit"
              className="btn btn-warning btn-block register-btn"
            >
              Register
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
