import React, { useEffect, useState } from "react";
import { profile } from "../api";
import ProfileComponent from "../components/ProfileComponent";

export default function Profile() {
  const [user, setUser] = useState([]);

  useEffect(() => {
    const id = localStorage.getItem("id");
    getUserProfile(id);
  }, []);

  const getUserProfile = async (id) => {
    try {
      const user = await profile(id);
      setUser([user.data.data]);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <ProfileComponent user={user} />
    </div>
  );
}
