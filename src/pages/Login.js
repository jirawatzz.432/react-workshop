import React, { useState, useEffect } from "react";
import { login } from "../api";

export default function Login(props) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [state, setstate] = useState("");

  useEffect(() => {
    localStorage.setItem("id", state || "");
  }, [state]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    let user = {
      username,
      password,
    };
    try {
      const result = await login(user);
      if (result.data.status === "success") {
        setstate(result.data.data._id);
        props.history.push("/home");
      }
    } catch (error) {}
  };

  return (
    <div>
      <div className="d-flex justify-content-center">
        <div className="col-md-6 login-form">
          <div className="text-center">
            <img
              src={process.env.PUBLIC_URL + "/images/lock.svg"}
              width="120"
              alt="login-img"
            />
          </div>
          <h3 className="text-center mb-3">Login</h3>
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Username</label>
              <input
                type="text"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                onChange={(e) => setUsername(e.target.value)}
              />
              <small id="emailHelp" className="form-text text-muted">
                We'll never share your username with anyone else.
              </small>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Password</label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
                onChange={(e) => setPassword(e.target.value)}
              />
            </div>
            <button
              type="submit"
              className="btn btn-primary btn-block login-btn"
            >
              Login
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}
