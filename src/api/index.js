import axios from "axios";

const url = "http://203.154.59.14:3000/api/v1";

export const login = (user) => axios.post(url + "/login", user);

export const register = (user) => axios.post(url + "/register", user);

export const profile = (id) => axios.get(url + "/users/" + id);

export const product = () => axios.get(url + "/products/");
