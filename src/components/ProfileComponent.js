import React from "react";

export default function ProfileComponent(props) {
  const { user } = props;
  return (
    <div>
      {user.map((user, index) => {
        return (
          <div class="card col-md-6 mt-5 shadow">
            <div class="card-body">
              <h5 class="card-title">My profile</h5>
              <p class="card-text">
                <strong>Name:</strong> {user.name}
              </p>
              <p class="card-text">
                <strong>Age:</strong> {user.age}
              </p>
              <p class="card-text">
                <strong>Salary:</strong> ฿{user.salary}
              </p>
              <hr />
              <a
                href="{{ route('profile.edit', $user->id)}}"
                class="btn btn-sm btn-outline-info"
              >
                Edit
              </a>
            </div>
          </div>
        );
      })}
    </div>
  );
}
