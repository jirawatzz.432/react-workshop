import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

export default function Header(props) {
  const [state, setstate] = useState(true);

  useEffect(() => {
    console.log("666");
    isLogin();

    console.log(state);
  }, [state]);

  const isLogin = async () => {
    console.log("555");
    if (localStorage.getItem("id")) {
      return await setstate(true);
    }
    await setstate(false);
  };

  const guest = (
    <ul className="navbar-nav ml-auto">
      <li className="nav-item">
        <NavLink
          to="/login"
          className="nav-link"
          activeStyle={{ color: "red" }}
        >
          Login
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          to="/register"
          className="nav-link"
          activeStyle={{ color: "red" }}
        >
          Register
        </NavLink>
      </li>
    </ul>
  );

  const user = (
    <ul className="navbar-nav ml-auto">
      <li className="nav-item">
        <NavLink
          to="/home"
          className="nav-link"
          activeStyle={{ color: "red" }}
          onClick={(e) => {
            setstate("");
            localStorage.removeItem("id");
          }}
        >
          Logout
        </NavLink>
      </li>
    </ul>
  );
  return (
    <div>
      <nav className="navbar navbar-light bg-light navbar-expand">
        <NavLink to="/home" className="navbar-brand">
          <img
            src={process.env.PUBLIC_URL + "/images/marijuana.svg"}
            width="30"
            height="30"
            className="d-inline-block align-top"
            alt="logo"
          />
          <span
            style={{ color: "#00A780", fontWeight: "900", marginLeft: "5px" }}
          >
            Green-shop
          </span>
        </NavLink>

        <div className="collapse navbar-collapse" id="navbarNav">
          {state ? user : guest}
        </div>
      </nav>
    </div>
  );
}
